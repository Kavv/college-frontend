import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CyclesComponent } from './views/cycles/cycles.component';
import { UsersComponent } from './views/users/users.component';
import { GroupsComponent } from './views/groups/groups.component';
import { LayoutsComponent } from './views/shared/layouts/layouts.component';
import { AreasComponent } from './views/areas/areas.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule} from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';

import { SharedModule } from './views/shared/shared.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import { UsersModule } from './views/users/users.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CyclesModule } from './views/cycles/cycles.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatFormFieldModule,
    MatMenuModule,
    UsersModule,
    NgxDatatableModule,
    CyclesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
