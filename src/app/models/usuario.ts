export class User {
    'id': number;
    'name': string;
    'code': string;
    'password': string;
    'gender': string;
    'birthday': string;
    'phone': string;
    'mother': string;
    'motherPhone': string;
    'father': string;
    'fatherPhone': string;
    constructor (){

    }
}
