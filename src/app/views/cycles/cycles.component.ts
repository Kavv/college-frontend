import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { trigger, transition, state, animate, style, AnimationEvent } from '@angular/animations';
import { DatatableComponent, ColumnMode  } from '@swimlane/ngx-datatable';
import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';



import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

interface userDto {
  code: string;
  name: string;
  createtAt: Date;  
}


@Component({
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
      })),
      transition('open => closed', [
        animate('0.2s')
      ]),
      transition('closed => open', [
        animate('0.2s')
      ]),
    ]),
  ],
  selector: 'app-cycles',
  templateUrl: './cycles.component.html',
  styleUrls: ['./cycles.component.scss']
})


export class CyclesComponent implements OnInit {
  
  constructor(public _userService: UsersService, public dialog: MatDialog) {
    this.allUsers(1);
    this.allUsers(2);
    this.allUsers(3);
  }
  
  hide = true;
  subMenu = true;
  email = new FormControl('', [Validators.required, Validators.email]);
  student = false;
  teacher = false;
  administrative = false;
  userOptions = true;
  isOpen1 = false;
  isOpen2 = false;
  isOpen3 = false;


  addStudent = false;
  addTeacher = false;
  addAdministrative = false;
  

  messageError: any[] = [{
    500 : "Ups... Ocurrió un error en el servidor",
    404 : "Ocurrió un error, no se encontro la página",
    403 : "Ups... No tiene suficientes permisos para realizar esta acción",
    400 : "Ha ocurrido un error, intentelo"
  }];

  ngOnInit(): void {
  }

  showSubModul(submodul:number)
  {
    (submodul == 0) ? this.subMenu = true : this.subMenu = false;
    (submodul == 1) ? this.student = true : this.student = false;
    (submodul == 2) ? this.teacher = true : this.teacher = false;
    (submodul == 3) ? this.administrative = true : this.administrative = false;
  }


  columns = [{ prop: 'code' }, { name: 'name' }, { prop: 'created_at' }];
  @ViewChild(DatatableComponent) table:any = DatatableComponent;
  @ViewChild(DatatableComponent) tableTeachers:any = DatatableComponent;
  @ViewChild(DatatableComponent) tableAdmin:any = DatatableComponent;

  ColumnMode = ColumnMode;
  students:userDto[] = [];
  teachers:userDto[] = [];
  administratives:userDto[] = [];
  tempEstudent:userDto[] = [];
  tempTeachers:userDto[] = [];
  tempAdmins:userDto[] = [];

  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempEstudent.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      (d.code.toLowerCase().indexOf(val) !== -1) || 
      (d.created_at.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.students = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }


  updateFilterTeachers(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempTeachers.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      (d.code.toLowerCase().indexOf(val) !== -1) || 
      (d.created_at.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.teachers = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableTeachers.offset = 0;
  }
  
  updateFilterAdmin(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempAdmins.filter(function (d:any) {
      return (d.name.toLowerCase().indexOf(val) !== -1) ||
      (d.code.toLowerCase().indexOf(val) !== -1) || 
      (d.created_at.indexOf(val) !== -1) || 
      !val;
    });

    // update the rows
    this.administratives = temp;
    // Whenever the filter changes, always go back to the first page
    this.tableAdmin.offset = 0;
  }



  allUsers(type:number){
    this._userService.students(type).subscribe((user:any) => {
      console.log(user);
      if(type == 1)
      {
        this.tempEstudent = [...user];
        return this.students = user;
      }
      if(type == 2)
      {
        this.tempTeachers = [...user];
        return this.teachers = user;
      }
      if(type == 3)
      {
        this.tempAdmins = [...user];
        return this.administratives = user;
      }
    });
  }

  statusError:number = 200;
  responseDialog:any;
  showUser(id:number, dialog:any){
    this._userService.show(id).subscribe((user:any) => {
      console.log(user);
        const dialogRef = this.dialog.open(dialog, {
          data: user
        });
        dialogRef.afterClosed().subscribe(response => {
          console.log(response);
          (response) ? this.refresh() : null;
        });
    },
    error => {
      this.statusError = error;
    });
  }

  //temp
  openDialog(option:number, id:number = 0)
  {

  }

  private refresh()
  {
    (this.student) ? this.allUsers(1) : null;
    (this.teacher) ? this.allUsers(2) : null;
    (this.administrative) ? this.allUsers(3) : null;
  }
/* 
  createUser(type:number)
  {
    const dialogRef = this.dialog.open(AddComponent, {
      data: {type: type}
    });
    dialogRef.afterClosed().subscribe(response => {
      console.log(response);
      (response) ? this.refresh() : null;
    });
  }

  openDialog(option:number, id:number = 0): void {
    
    if(option == 1)
    {
      // por defecto es estudiante (1)
      let type:number = 1;
      (this.teacher) ? type = 2 : null;
      (this.administrative) ? type = 3 : null;
      this.createUser(type);
    }
    if(option == 2)
      this.showUser(id, EditComponent);
    if(option == 3)
      this.showUser(id, DeleteComponent);
    if(option == 4)
      this.showUser(id, DetailComponent);
    
  } */
}

