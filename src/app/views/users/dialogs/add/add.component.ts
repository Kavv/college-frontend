import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {


  ngOnInit(): void {
  }
  
  userForm:any;

  constructor(
    public dialogRef: MatDialogRef<AddComponent>,
    public _userService:UsersService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.userForm = formBuilder.group({
        code: [''],
        name: [''],
        password: ['HispanoAmericano'],
        gender: [''],
        birthday: [''],
        phone: [''],
        mother: [''],
        motherPhone: [''],
        father: [''],
        fatherPhone: [''],
        type: [data.type]
      });
      console.log(this.userForm);
    }
  
  
    hide = false;
    selectedValue:string = "";
  
    messageError: any[] = [{
      500 : "Ups... Ocurrió un error en el servidor",
      404 : "Ocurrió un error, no se encontro la página",
      403 : "Ups... No tiene suficientes permisos para realizar esta acción",
      400 : "Ha ocurrido un error, intentelo"
    }];
  
    saveUser(){
      this._userService.store(this.userForm.value).subscribe((response:any) => {
        this.dialogRef.close(true);
        this.openSnackBar("¡Usuario agregado exitosamente!", "style-success");
      },
      error => {
        this.openSnackBar(this.messageError[0][error], "style-error");
      });
    }
  
    horizontalPosition: MatSnackBarHorizontalPosition = 'start';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    openSnackBar(msg:string, type:string) {
      this._snackBar.open(msg, 'Cerrar', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: [type],
      });
    }
  
}
