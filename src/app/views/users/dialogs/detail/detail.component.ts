import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';

import { UsersService } from 'src/app/services/users/users.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {


  ngOnInit(): void {
  }


  userForm:any;
  private idStudent:number;

  constructor(
    public dialogRef: MatDialogRef<DetailComponent>,
    public _userService:UsersService,
    private formBuilder: FormBuilder,



    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      console.log(data.student.length);
      this.idStudent = data.id;
      if(data.student.length > 0)
      {
        this.userForm = formBuilder.group({
          code: [{value: data.code, disabled: false}],
          name: [{value: data.name, disabled: false}],
          password: [{value: '', disabled: false}],
          gender: [{value: data.student[0].gender, disabled: false}],
          birthday: [{value: data.student[0].birthday, disabled: false}],
          phone: [{value: data.student[0].student_phone, disabled: false}],
          mother: [{value: data.student[0].mother_name, disabled: false}],
          motherPhone: [{value: data.student[0].mother_phone, disabled: false}],
          father: [{value: data.student[0].father_name, disabled: false}],
          fatherPhone: [{value: data.student[0].father_phon, disabled: false}]
        });
      }
      else
      {
        this.userForm = formBuilder.group({
          code: [{value: data.code, disabled: false}],
          name: [{value: data.name, disabled: false}],
          password: [{value: '', disabled: false}],
          gender: [{value: '', disabled: false}],
          birthday: [{value: '', disabled: false}],
          phone: [{value: '', disabled: false}],
          mother: [{value: '', disabled: false}],
          motherPhone: [{value: '', disabled: false}],
          father: [{value: '', disabled: false}],
          fatherPhone: [{value: '', disabled: false}]
        });
      }
    }    

}
