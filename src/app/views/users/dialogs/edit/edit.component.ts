import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

import { UsersService } from 'src/app/services/users/users.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { FormBuilder } from '@angular/forms'

interface userDto {
  code:string;
  name:string;
  gender:string;
  birthday:string;
  phone:string;
  mother:string;
  motherPhone:string;
  father:string;
  fatherPhone:string;
}

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  host: {'class': 'main-dialog'},
})
export class EditComponent implements OnInit {


  ngOnInit(): void {
  }


  userForm:any;
  private idStudent:number;

  constructor(
    public dialogRef: MatDialogRef<EditComponent>,
    public _userService:UsersService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,



    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      console.log(data.student.length);
      this.idStudent = data.id;
      if(data.student.length > 0)
      {
        this.userForm = formBuilder.group({
          code: [data.code],
          name: [data.name],
          password: [{value: '', disabled: true}],
          gender: [data.student[0].gender],
          birthday: [data.student[0].birthday],
          phone: [data.student[0].student_phone],
          mother: [data.student[0].mother_name],
          motherPhone: [data.student[0].mother_phone],
          father: [data.student[0].father_name],
          fatherPhone: [data.student[0].father_phon]
        });
      }
      else
      {
        this.userForm = formBuilder.group({
          code: [data.code],
          name: [data.name],
          password: [],
          gender: [''],
          birthday: [''],
          phone: [''],
          mother: [''],
          motherPhone: [''],
          father: [''],
          fatherPhone: ['']
        });
      }
    }    

  
    hide = false;
    selectedValue:string = "";
  
    messageError: any[] = [{
      500 : "Ups... Ocurrió un error en el servidor",
      404 : "Ocurrió un error, no se encontro la página",
      403 : "Ups... No tiene suficientes permisos para realizar esta acción",
      400 : "Ha ocurrido un error, intentelo"
    }];
  
    

    updateUser(){
      this._userService.update(this.userForm.value, this.idStudent).subscribe((response:any) => {
        this.dialogRef.close(true);
        this.openSnackBar("¡Usuario editado exitosamente!", "style-success");
      },
      error => {
        this.openSnackBar(this.messageError[0][error], "style-error");
      });
    }
  
    horizontalPosition: MatSnackBarHorizontalPosition = 'start';
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    openSnackBar(msg:string, type:string) {
      this._snackBar.open(msg, 'Cerrar', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: [type],
      });
    }

    restartPass(){
      this.userForm.controls['password'].setValue("HispanoAmericano");
    }

}
