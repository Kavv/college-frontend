import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { AuthService } from './auth.service';

interface UsuarioDto {
  user: string;
  name: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'college-client';
  usuarios: UsuarioDto[] = [];
  constructor(private authService: AuthService){

  }
  
  ngOnInit(): void{
    
  }
}
