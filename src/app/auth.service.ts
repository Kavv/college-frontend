import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  base = environment.baseURL;
  
  constructor(private http: HttpClient) { }

  
  public getUser(){
    return this.http.get(`${this.base}/users`);
  }
}
