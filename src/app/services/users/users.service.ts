import { Injectable } from '@angular/core';
import { User } from 'src/app/models/usuario';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }
  
  base = environment.baseURL;
  public all(){
    return this.http.get(`${this.base}/users/`).pipe(catchError(this.errorHandler));
  }
  public students(type:number){
    return this.http.get(`${this.base}/student_list/`+type).pipe(catchError(this.errorHandler));
  }
  public show(id:number){
    return this.http.get(`${this.base}/users/`+id).pipe(catchError(this.errorHandler));
  }
  
  public store(data:any){
    return this.http.post(`${this.base}/users`, data).pipe(catchError(this.errorHandler));
  }
  
  public update(data:any, id:number){
    return this.http.put(`${this.base}/users/`+id, data).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.status);
  }
  
  public delete(id:number){
      return this.http.delete(`${this.base}/users/`+id).pipe(catchError(this.errorHandler));
  }
}

